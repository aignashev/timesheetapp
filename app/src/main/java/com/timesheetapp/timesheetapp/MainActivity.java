package com.timesheetapp.timesheetapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.ui.auth.core.FirebaseLoginBaseActivity;
import com.firebase.ui.auth.core.FirebaseLoginError;
import com.firebase.ui.auth.core.SocialProvider;
import com.firebase.ui.auth.core.TokenAuthHandler;

import java.util.Date;

public class MainActivity extends FirebaseLoginBaseActivity {
    private static final String TAG = "TimeSheetApp";
    static String worktime_progress = "worktime_progress";
    SharedPreferences pref;
    Boolean status = false;
    Button button = null;
    Button logOffButton = null;
    Button logInButton = null;
    Firebase ref;
    Boolean loggedIn;
    String userName;
    String loginToken;
    String email;

    @Override
    protected void onStart() {
        super.onStart();
        setEnabledAuthProvider(SocialProvider.password);

        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.button);
        logOffButton = (Button) findViewById(R.id.button3);
        logInButton = (Button) findViewById(R.id.button2);


        pref = getApplicationContext().getSharedPreferences("TimeSheetAppStatus", 0); // 0 - for private mode
        status = pref.getBoolean(worktime_progress, false);
        loggedIn = pref.getBoolean("logged_in", false);

        updateButtonName();
        button.setOnClickListener(new AdapterView.OnClickListener() {

            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = pref.edit();
                updateData();
                editor.putBoolean(worktime_progress, !pref.getBoolean(worktime_progress, false));
                editor.commit();
                status = pref.getBoolean(worktime_progress, false);
                updateButtonName();
            }
        });
        logOffButton.setOnClickListener(new AdapterView.OnClickListener() {

            @Override
            public void onClick(View v) {
                logout();
            }
        });
        logInButton.setOnClickListener(new AdapterView.OnClickListener() {

            @Override
            public void onClick(View v) {
                showFirebaseLoginPrompt();
            }
        });

    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        button = (Button) findViewById(R.id.button);
        // Use Firebase to populate the list.
//        Firebase.setAndroidContext(this);
//        pref = getApplicationContext().getSharedPreferences("TimeSheetAppStatus", 0); // 0 - for private mode
//        status = pref.getBoolean(worktime_progress, false);
//        ref = new Firebase("https://timesheetapp.firebaseio.com/TimeEvents");



//        updateButtonName();

//        button.setOnClickListener(new AdapterView.OnClickListener() {

//            @Override
//            public void onClick(View v) {
//                SharedPreferences.Editor editor = pref.edit();
//                updateData();
//                editor.putBoolean(worktime_progress, !pref.getBoolean(worktime_progress, false));
//                editor.commit();
//                status = pref.getBoolean(worktime_progress, false);
//                updateButtonName();
//            }
//        });
//
//    }

    private void updateButtonName() {
        if (status) {
            button.setText("FINISH");
        } else {
            button.setText("START");
        }
    }

    private void updateData() {
        String eventType = "START";
        if (status) {
            eventType = "FINISH";
        }
        TimeEvent te = new TimeEvent();
        te.setUserEmail(email);
        te.setEventType(eventType);
        te.setDateTime(new Date());
        Firebase rf = getFirebaseRef();
        rf.push().setValue(te);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    protected void onFirebaseLoggedIn(AuthData authData) {
//        dismissFirebaseLoginPrompt();
                logInButton.setEnabled(false);
                logOffButton.setEnabled(true);
                SharedPreferences.Editor editor = pref.edit();
                updateData();
                editor.putString("email", email);
                editor.putString("username", userName);
                editor.putString("loginToken", loginToken);
                editor.commit();
                status = pref.getBoolean(worktime_progress, false);
        Log.i(TAG, "Logged in to " + authData.getProvider().toString());

        switch (authData.getProvider()) {
            case "password":
                email = (String) authData.getProviderData().get("email");
                break;
            default:
                email = (String) authData.getProviderData().get("displayName");
                break;
        }

    }

    @Override
    public void onFirebaseLoggedOut() {
        logInButton.setEnabled(true);
        logOffButton.setEnabled(false);
        Log.i(TAG, "Logged out");
        email = "";
        invalidateOptionsMenu();
    }

    @Override
    public void onFirebaseLoginProviderError(FirebaseLoginError firebaseError) {
        Log.i(TAG, "Login provider error: " + firebaseError.toString());
    }

    @Override
    public void onFirebaseLoginUserError(FirebaseLoginError firebaseError) {
        resetFirebaseLoginDialog();
        Log.i(TAG, "Login user error: " + firebaseError.toString());
    }


    @Override
    protected Firebase getFirebaseRef() {
        if (ref == null) {
            Firebase.setAndroidContext(this);
            ref = new Firebase("https://timesheetapp.firebaseio.com/TimeEvents");
        }
        return ref;
    }
}
