package com.timesheetapp.timesheetapp;

import java.util.Date;

/**
 * Created by ignashev on 1/1/16.
 */
public class TimeEvent {
    String userEmail;
    String eventType;
    Date dateTime;

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getEventType() {
        return eventType;
    }

    public Date getDateTime() {
        return dateTime;
    }
}
